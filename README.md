# Magang-DISKOMINFOSANDI BARITO UTARA

Saya Retno Ayu Wulandari (2000016063) saat ini magang di Dinas Komunikasi, Informatika, dan Persandian Kabupaten Barito Utara. Saat ini saya diberi tugas sebagai System Analyst dalam pengembangan Website Laporan Kegiatan. Saya magang dimulai dari tanggal 1 Maret 2023 - 1 Juli 2023. Selain sebagai system analyst disini saya juga membantu persiapan pemantauan evaluasi SPBE Kabupaten Barito Utara.


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Retnoayuwulandari/magang-diskominfosandi-barito-utara/-/settings/integrations)


## Project status
On Process
